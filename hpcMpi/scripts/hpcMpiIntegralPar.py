#!/usr/bin/env python3

import hpcMpi
import sys
import time as t
from mpi4py import MPI
import numpy


if __name__ == '__main__':

    # parse command line arguments
    step = 1e-3
    if len(sys.argv) == 2:
        step = float(sys.argv[1])

    comm = MPI.COMM_WORLD
    worldRank = comm.Get_rank()
    worldSize = comm.Get_size()

    t0 = t.time()

    a = worldRank/worldSize
    b = (worldRank+1)/worldSize
    value = hpcMpi.compute(hpcMpi.fPi, a, b, step)


    node_data = value
    node_result = numpy.empty(1, 'i')
    node_result[0] = node_data*node_data
    all_results = numpy.empty(1, 'i')
    comm.Reduce(node_result, all_results, op=MPI.SUM)

    t1 = t.time()

    time = t1 - t0
    if worldRank == 0:
        print(all_results[0])
    

    # output result
    print(step, "1", value, time)
